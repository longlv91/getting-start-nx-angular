pipeline{
  agent any
  tools {
    nodejs 'NodeJS 8.10.0'
  }
  options {
    gitLabConnection('webframework-gitlab')
  }
  triggers {
      gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
  }
  stages{
    stage('Prepare'){
      steps{
        withNPM(npmrcConfig:'npm-settings-default'){
          // Use 'npm ci' in future
          // https://docs.npmjs.com/cli/ci
          sh 'npm install'
        }
      }
    }
    stage('Lint'){
      steps{
        sh 'npm run lint'
      }
    }
    stage('Test'){
      steps{
        echo 'Test disabled'
        // Temporary disable until jest is broken
        // sh 'npm run test'

        // publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'coverage', reportFiles: 'index.html', reportName: 'Code Coverage', reportTitles: ''])
        // junit 'junit.xml'
      }
    }
    stage('Build'){
      steps{
        sh 'npm run build'
      }
    }
    stage('Release Canary'){
      when{
       expression { return env.BRANCH_NAME ==~ /.*-canary/ }
      }
      steps{
        withNPM(npmrcConfig:'npmrc-canary') {
          sh 'npm run pub'
        }
      }
    }
    stage('Final Release'){
      when{
       expression { return !(env.BRANCH_NAME ==~ /.*-canary/) }
      }
      steps{
        withNPM(npmrcConfig:'npmrc-final') {
          sh 'npm run release'
        }
      }
    }
  }
  post {
    failure {
      updateGitlabCommitStatus name: 'build', state: 'failed'
    }
    success {
      updateGitlabCommitStatus name: 'build', state: 'success'
    }
  }
}

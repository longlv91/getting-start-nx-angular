import { Component } from '@angular/core';

/**
 * The root component of the application
 */
@Component({
  selector: 'gs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class ApplicationComponent {}

import { TestBed, async } from '@angular/core/testing';
import { ApplicationComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { MasterPageModule } from '@webframework/client-master-page';
describe('AppComponent', () => {
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [RouterModule, MasterPageModule],
        declarations: [ApplicationComponent]
      }).compileComponents();
    })
  );
  it(
    'should create the app',
    async(() => {
      const fixture = TestBed.createComponent(ApplicationComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    })
  );
  it(
    `should have as title 'get-start'`,
    async(() => {
      const fixture = TestBed.createComponent(ApplicationComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app.title).toEqual('get-start');
    })
  );
  it(
    'should render title in a h1 tag',
    async(() => {
      const fixture = TestBed.createComponent(ApplicationComponent);
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('h1').textContent).toContain(
        'Welcome to dashboard!'
      );
    })
  );
});

import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@webframework/client-core';
import { LayoutContainerComponent } from '@webframework/client-layout';

export const APPLICATION_ROUTES: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: ':layout',
    component: LayoutContainerComponent,
    canActivate: [AuthGuard]
  },

  // Dummy paths for lazy loaded modules
  //
  // Update tsconfig.app.json for each lazy loaded module
  // https://github.com/nrwl/nx/issues/528
  { path: '-', loadChildren: '@get-start/user#UserModule' },
  { path: '-', loadChildren: '@get-start/data#DataModule' }
];

export const ApplicationRoutes: ModuleWithProviders = RouterModule.forRoot(
  APPLICATION_ROUTES,
  { useHash: false }
);

import { Component } from '@angular/core';

@Component({
  selector: 'get-start-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'get-start';
}

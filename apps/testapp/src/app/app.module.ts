import { APP_BASE_HREF } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import {
  RouterStateSerializer,
  StoreRouterConnectingModule
} from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  CoreModule,
  CustomRouterStateSerializer,
  RouterEffects,
  SessionEffects
} from '@webframework/client-core';
import { LayoutModule } from '@webframework/client-layout';
import { MasterPageModule } from '@webframework/client-master-page';
import { IconModule } from '@webframework/client-shared';
import { AuthenticationService } from '@webframework/client-core';

import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
// import { metaReducers, reducers } from './reducers';

import { RouterModule, Routes } from '@angular/router';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CoreModule.forRoot(),
    LayoutModule,
    IconModule.forRoot(),
    MasterPageModule,
    BrowserAnimationsModule,
    RouterModule,

    StoreModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreRouterConnectingModule,
    EffectsModule.forRoot([RouterEffects,SessionEffects])
  ],
  providers: [
    { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer },
    { provide: AuthenticationService, useClass: AuthenticationService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

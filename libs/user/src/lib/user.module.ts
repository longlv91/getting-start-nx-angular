import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UserComponent } from './components/user/user.component';

/**
 * User Module
 */
@NgModule({
  imports: [CommonModule],
  declarations: [UserComponent],
  entryComponents: [UserComponent],
  exports: [UserComponent]
})
export class UserModule {
  public static entry = UserComponent;
}

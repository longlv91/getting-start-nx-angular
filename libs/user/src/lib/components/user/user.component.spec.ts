import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SessionService } from '@webframework/client-core';
import { of } from 'rxjs';

import { UserComponent } from './user.component';

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async(() => {
    const MockSessionService = {
      onReady$: of({})
    };

    TestBed.configureTestingModule({
      declarations: [UserComponent],
      providers: [
        { provide: SessionService, useValue: MockSessionService }
      ]
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should have user', () => {
    fixture.detectChanges();
    expect(component.user).toBeTruthy();
  });
});

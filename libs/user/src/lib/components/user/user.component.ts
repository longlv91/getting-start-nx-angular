import { Component, Inject, OnInit, Optional } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  BroadcastService,
  User,
  SessionService
} from '@webframework/client-core';
import { PANEL_CONFIG, panelActions } from '@webframework/client-layout';

/**
 * A Webframework Component which displays the user info
 */
@Component({
  selector: 'gs-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  greeting = 'Hello';

  /**
   * User object
   */
  public user: User;

  /**
   * @param  {SessionService} An instance of {@link SessionService}
   */
  constructor(
    @Inject(PANEL_CONFIG) config,
    private sessionService: SessionService,
    private broadcastService: BroadcastService,
    private store: Store<any>
  ) {
    console.log(config);
    store.dispatch(new panelActions.Restore('layout:2'));
  }
  /**
   * Subscribe to {@link SessionService}'s onReady event and update user info
   */
  ngOnInit() {
    this.sessionService.onReady$.subscribe(user => (this.user = user));
  }

  dispatch() {
    this.store.dispatch(new panelActions.Restore('layout:2'));
  }
}

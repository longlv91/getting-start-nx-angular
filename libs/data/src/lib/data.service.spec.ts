import { TestBed, inject } from '@angular/core/testing';

import { Http } from '@angular/http';

import { DataService } from './data.service';
import { SessionService } from '@webframework/client-core';
import { of } from 'rxjs';

describe('DataService', () => {
  beforeEach(() => {
    const MockSessionService = {
      getResource: () => of({})
    };

    TestBed.configureTestingModule({
      providers: [
        DataService,
        { provide: SessionService, useValue: MockSessionService }
      ]
    });
  });

  it(
    'should create',
    inject([DataService], (service: DataService) => {
      expect(service).toBeTruthy();
    })
  );

  it(
    'should return user data',
    inject([DataService], (service: DataService) => {
      service.getData().subscribe(data => {
        expect(data).toBeTruthy();
      });
    })
  );
});

import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as data from '../actions/data';
import { DataService } from '../data.service';
import { Data } from '../models/data';

@Injectable()
export class DataEffects {
  @Effect()
  loadData$: Observable<Action> = this.action$.ofType(data.LOAD)
    .pipe(
      switchMap(() => this.dataService.getData()),
      map((d: Data) => new data.LoadSuccess(d)),
      catchError(error => of(new data.LoadFail(error))));

  constructor(private action$: Actions, private dataService: DataService) {}
}

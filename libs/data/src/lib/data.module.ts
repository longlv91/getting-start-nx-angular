import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataComponent } from './components/data/data.component';

/**
 * An example module which exports {@link DataComponent}
 */
@NgModule({
  declarations: [DataComponent],
  entryComponents: [DataComponent],
  imports: [CommonModule],
  exports: [DataComponent]
})
export class DataModule {
  public static entry = DataComponent;
}

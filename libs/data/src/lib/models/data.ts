/**
 * Data Model
 *
 * @export
 * @interface Data
 */
export interface Data {
  /**
   * Text
   *
   * @type {string}
   * @memberof Data
   */
  text: string;

  /**
   * Message
   *
   * @type {string}
   * @memberof Data
   */
  message: string;

  /**
   * Status code
   *
   * @type {number}
   * @memberof Data
   */
  statusCode: number;
}

import { Injectable } from '@angular/core';
import { WebFrameworkService } from '@webframework/client-core';
import { Observable } from 'rxjs';

import { Data } from './models/data';

/**
 * Data Service
 * Performs calls to backend services to fetch example data
 */
@Injectable()
export class DataService {
  /**
   * Creates an instance of DataService.
   * @param {SessionService} sessionService
   *
   * @memberOf DataService
   */
  constructor(private webframeworkService: WebFrameworkService) {}

  /**
   * This is the http call to the middle tier service
   * @public
   * @returns Observable of {@link IData}
   */
  public getData(): Observable<Data> {
    return this.webframeworkService.getResource<Data>('/getdatafromservice');
  }
}

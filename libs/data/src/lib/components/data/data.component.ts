import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BroadcastService, SessionService } from '@webframework/client-core';
import { Observable } from 'rxjs';

import { DataService } from '../../data.service';
import { Data } from '../../models/data';

/**
 * An example Web Framework component
 *
 * Displays data from backend example service in tabular format
 */
@Component({
  selector: 'gs-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss'],
  providers: [DataService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataComponent {
  /**
   * Data fetched from the backend service
   */
  public data$: Observable<Data>;

  /**
   * Flag to show broadcast message
   */
  public showBroadcastMessage = false;

  /**
   * Constructor
   * @param dataService Instance of DataService
   * @param sessionService Instance of SessionService
   * @param broadcastService Instance of BroadcastService
   */
  constructor(
    private dataService: DataService,
    private sessionService: SessionService,
    public broadcastService: BroadcastService
  ) {
    this.data$ = this.dataService.getData();

    broadcastService.on('menuEvent').subscribe(data => {
      this.showBroadcastMessage = true;
    });
  }
}

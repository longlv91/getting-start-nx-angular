import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SessionService } from '@webframework/client-core';

import { of } from 'rxjs';

import { DataService } from '../../data.service';
import { DataComponent } from './data.component';

describe('DataComponent', () => {
  let component: DataComponent;
  let fixture: ComponentFixture<DataComponent>;

  beforeEach(async(() => {
    const MockSessionService = {
      onReady$: of({}),
      getResource: () => of({})
    };

    TestBed.configureTestingModule({
      declarations: [DataComponent],
      providers: [
        { provide: SessionService, useValue: MockSessionService }
      ]
    })

    beforeEach(() => {
      fixture = TestBed.createComponent(DataComponent);
      component = fixture.componentInstance;
    });

    it('should create', () => {
      fixture.detectChanges();
      expect(component).toBeTruthy();
    });

    it('should have data', () => {
      fixture.detectChanges();
      expect(component.data$).toBeTruthy();
    });
  }))
})

import { Data } from '../models/data';

export interface DataState {
  data: Data;
}

export const initialState: DataState = {
  data: null
};

// export function reducer

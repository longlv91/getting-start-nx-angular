import { Action } from '@ngrx/store';

import { IData as Data } from '../models/data';

export const LOAD = '[Data] Load';
export const LOAD_SUCCESS = '[Data] Load Success';
export const LOAD_FAIL = '[Data] Load Fail';

/**
 * Load Data Actions
 */
export class Load implements Action {
  readonly type = LOAD;

  constructor() {}
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: Data) {}
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) {}
}

export type Actions = Load | LoadSuccess | LoadFail;

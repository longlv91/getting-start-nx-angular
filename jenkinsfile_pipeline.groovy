#!groovy

node {
    currentBuild.result = "SUCCESS"
    
    // NodeJS Configuration
    def npmrcDefaultConfig = 'npm-settings-default'
    def nodejsName = 'nodejs'
    
    // Git Configurations
    def gitUrl = "git@${GIT_SERVER}:${GIT_GROUP}/${GIT_REPO}.git"
    def gitCredentials = "${GIT_CREDENTIALS}"
    def gitUserName = "${GIT_CI_USER}"
    def gitUserEmail = "${GIT_CI_USER}@${GIT_SERVER}"
    
    // Branch Configurations

    def publishRegistryConfig = ("${APP_GIT_BRANCH}" == 'develop') ? "npm-develop" : "npm-master"
    def artifactoryUrl="${ARTIFACTORY_SERVER}/${publishRegistryConfig}"

    def workingBranch = "${APP_GIT_BRANCH}"
    def developBranch = "${GIT_DEV_BRANCH}"

    
    echo ">>> Build started: ${env.BUILD_NUMBER}"

    tool name: nodejsName, type: 'nodejs'

    try {
        nodejs(configId: npmrcDefaultConfig, nodeJSInstallationName: nodejsName) {
            stage('Checkout') {
                checkout([
                    $class: 'GitSCM',
                    branches: [[name: "*/${workingBranch}"]],
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [
                        [$class: 'LocalBranch', localBranch: workingBranch],
                        [$class: 'UserExclusion', excludedUsers: gitUserName],
                        [$class: 'WipeWorkspace']
                    ],
                    submoduleCfg: [],
                    userRemoteConfigs: [[
                        credentialsId: gitCredentials,
                        url: gitUrl
                    ]]
                ])
            }
            stage('Prepare'){
                     withNPM(npmrcConfig: npmrcDefaultConfig) {
                    sh 'npm config list'
                    sh 'npm install'

            }
            }
        
            stage('Build') {
                sh 'npm run build'
            }

            stage('Publishing') {
                withNPM(npmrcConfig: publishRegistryConfig) {
                    sh 'npm publish'
                }
            }
            stage('Publishing ZIP in REPO') {
            
            def server = Artifactory.newServer url: artifactoryUrl,credentialsId: ARTIFACTORY_CREDENTIALS
            def buildInfo=Artifactory.newBuildInfo()
            def uploadSpec = """{
            "files": [
                {
                "pattern": "./*.tar",
                "target": "gettingstarted-builds/container/"
                }
            ]
            }"""
            server.upload spec: uploadSpec, buildInfo: buildInfo
            }
            stage('Bump version') {
                sh 'git config --global user.email ' + gitUserEmail
                sh 'git config --global user.name ' + gitUserName
                if ("${APP_GIT_BRANCH}" == 'master') {
                    sshagent([gitCredentials]) {
                        sh 'npm version patch -m "[ci-skip] Version %s"'
                        sh 'git push --tags origin HEAD:' + workingBranch
                    }
                }
                if ("${APP_GIT_BRANCH}" == 'develop') {
                    sshagent([gitCredentials]) {
                        sh 'npm version prerelease -m "[ci-skip] Version %s"'
                    sh 'git push --tags origin HEAD:' + workingBranch
                    }
                }                 
            }
               
           
            
            
            stage('Sync') {
                if ("${APP_GIT_BRANCH}" == 'master') {
                    sshagent([gitCredentials]) {
                        sh 'npm version prerelease -m "[ci-skip] Version %s"'
                        sh 'git push --tags origin HEAD:' + developBranch
                    }
                }
            }
        }
    } catch (err) {
        currentBuild.result = "FAILED"
        throw err
    }
}

pipelineJob("Examples/WebFramework/${APP_GIT_BRANCH}/${GIT_REPO}") {
    scm {
        git {
            remote {
                url "git@${GIT_SERVER}:${GIT_GROUP}/${GIT_REPO}.git"
                credentials "${GIT_CREDENTIALS}"
            }
            branch "${APP_GIT_BRANCH}"
        }
    }

    environmentVariables {
        env('GIT_SERVER', "${GIT_SERVER}")
        env('GIT_GROUP', "${GIT_GROUP}")
        env('GIT_REPO', "${GIT_REPO}")
        env('GIT_CREDENTIALS', "${GIT_CREDENTIALS}")
        env('GIT_CI_USER', "${GIT_CI_USER}")
        env('APP_GIT_BRANCH', "${APP_GIT_BRANCH}")
        env('GIT_DEV_BRANCH', "${GIT_DEV_BRANCH}")
        env('ARTIFACTORY_SERVER',"${ARTIFACTORY_SERVER}")
        env('ARTIFACTORY_CREDENTIALS',"${ARTIFACTORY_CREDENTIALS}")
    }

    triggers {
        scm 'H/5 * * * *'
    }
    
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url "git@${GIT_SERVER}:${GIT_GROUP}/${GIT_REPO}.git"
                        credentials "${GIT_CREDENTIALS}"
                    } 
                    branch "${APP_GIT_BRANCH}"
                    configure { node ->
                        node / 'extensions' << 'hudson.plugins.git.extensions.impl.UserExclusion' {
                            excludedUsers "${GIT_CI_USER}"
                        }
                    }
                }
            }
            scriptPath('jenkinsfile_pipeline.groovy')
        }
    }
}

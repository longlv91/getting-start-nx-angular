const gulp = require('gulp');
const tar = require('gulp-tar');
const version = require('./package').version;
gulp.task('default', () =>
    gulp.src('dist/**')
        .pipe(tar(`getting-started-${version}.tar`))
        .pipe(gulp.dest('./'))
);